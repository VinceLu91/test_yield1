def call_block(name)
  puts 'Start of method'  
  # you can call the block using the yield keyword  
  yield  
  yield  
  puts 'End of method' 
  puts 'Hi '  + name
end  
# Code blocks may appear only in the source adjacent to a method call  
call_block {puts 'In the block'}  
call_block { puts "#{4 + 8}" }
call_block('Vince') # Argument error